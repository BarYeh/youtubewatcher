
var idCounter = 0;
var width = 480;
var height = 270;
var widthMainInput;
var heightMainInput;
var modalTop;
var urlInput;
const frames = {};
const modalWindowHolderStyle = "display:flex;justify-content:center;align-items:center;position:absolute;width:100%;height:100%;top:0px;"
const modalWindowStyle = "width:350px;height:200px;background:white;border:solid 1px;"
const modalBackgroundStyle = "display:flex;position:absolute;width:100%;height:100%;background:black;opacity:0.3;top:0px;left:0px;"
const topStyle = "display:flex;align-items:baseline;justify-content:center;margin:20px;";
const frameParentStyle = 'width:fit-content;border:solid 1px;margin:10px;background:#f0f0f0;';

const changeMainWidth = () => {
    width = parseInt(widthMainInput.value);
    Object.keys(frames).forEach(key => {
        const frame = frames[key];
        if (!frame.width) frame.element.setAttribute('width', width)
    });
};

const changeMainHeight = () => {
    height = parseInt(heightMainInput.value);
    Object.keys(frames).forEach(key => {
        const frame = frames[key];
        if (!frame.height) frame.element.setAttribute('height', height)
    });
};

const changeWidth = (id) => {
    const frame = frames[id];
    const myWidth = parseInt(frame.widthInput.value);
    frame.width = myWidth;
    frame.element.setAttribute('width', myWidth)
}

const changeHeight = (id) => {
    const frame = frames[id];
    const myHeight = parseInt(frame.heightInput.value);
    frame.height = myHeight;
    frame.element.setAttribute('height', myHeight)
}

const toDefaults = (id) => {
    const frame = frames[id];
    delete frame.width;
    delete frame.height;
    frame.element.setAttribute('height', height);
    frame.element.setAttribute('width', width);
    frame.widthInput.value = '';
    frame.heightInput.value = '';
}

const clicked = () => {
    makeIframWindow();
};

const removeModal = () => {
    modalTop.remove();
}

const removeFrame = (id) => {
    const frame = frames[id];
    frame.parent.remove();
    delete frames[id];
}

const add = (tag, parent, attributes, otherAttributes = {}) => {
    var element = document.createElement(tag);
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
    Object.keys(otherAttributes).forEach(key => element[key] = otherAttributes[key]);
    parent.appendChild(element);
    return element;
};

const makeModal = () => {
    modalTop = add('div', document.body, {});
    add(
        'div', modalTop, {},
        { style: modalBackgroundStyle }
    );
    const modalHolder = add(
        'div', modalTop, {},
        { style: modalWindowHolderStyle }
    );
    const modalWindow = add(
        'div', modalHolder, {},
        { style: modalWindowStyle }
    );
    add(
        'input', modalWindow,
        {
            type: 'button',
            value: 'X',
            onclick: 'removeModal()'
        },
        { style: 'margin:10px;float:right;' }
    );
    const modalTopDiv = add('div', modalWindow, {}, { style: topStyle });
    const modalBotDiv = add('div', modalWindow, {}, { style: topStyle });
    add('div', modalTopDiv, {}, { innerHTML: 'video url:' });
    urlInput = add(
        'input', modalTopDiv,
        {
            type: 'text'
        },
        { style: 'margin:10px;' }
    );
    add(
        'input',
        modalBotDiv,
        {
            type: 'button',
            value: 'add',
            name: 'add_button',
            onclick: 'confirmUrl()'
        }
    );
};

const confirmUrl = () => {
    const urlParts = urlInput.value.split('/watch?v=');
    if (urlParts.length !== 2) {
        alert("Bad url! Please try again.");
        urlInput.value = '';
    } else {
        const url = `${urlParts[0]}/embed/${urlParts[1]}`;
        makeIframWindow(url);
    }
}

const makeIframWindow = (url) => {
    const parent = add(
        'div', document.getElementById('bot'), {},
        { style: frameParentStyle }
    );
    add(
        'input', parent,
        {
            type: 'button',
            value: 'X',
            onclick: `removeFrame(${idCounter})`
        },
        { style: 'margin:10px;float:right;' }
    );
    const top = add('div', parent, {}, { style: topStyle });
    add('div', top, {}, { innerHTML: 'height:' });
    const heightInput = add(
        'input',
        top,
        {
            type: 'number',
            onchange: `changeHeight(${idCounter})`
        },
        { style: 'margin:10px;width:50px;' }
    );
    add('div', top, {}, { innerHTML: 'width:' });
    const widthInput = add(
        'input',
        top,
        {
            type: 'number',
            onchange: `changeWidth(${idCounter})`
        },
        { style: 'margin:10px;width:50px;' }
    );
    add(
        'input',
        top,
        {
            type: 'button',
            value: 'to defaults',
            onclick: `toDefaults(${idCounter})`
        },
        { style: 'margin:10px;' }
    );
    const bot = add(
        'div',
        parent, {},
        { style: "display:flex;justify-content:center;" }
    );
    const element = add(
        'iframe',
        bot,
        {
            id: `frame-${idCounter}`,
            width,
            height,
            src: url
        },
        { style: 'margin:10px;' }
    )
    frames[idCounter] = { parent, element, heightInput, widthInput };
    removeModal();
    idCounter++;
}

const makeMainPage = () => {
    const top = document.getElementById('top');
    top.style = topStyle;
    add('div', top, {}, { innerHTML: 'height:' });
    heightMainInput = add(
        'input',
        top,
        {
            type: 'number',
            value: height,
            onchange: 'changeMainHeight()'
        },
        { style: 'margin:10px;' }
    );
    add('div', top, {}, { innerHTML: 'width:' });
    widthMainInput = add(
        'input',
        top,
        {
            type: 'number',
            value: width,
            onchange: 'changeMainWidth()'
        },
        { style: 'margin:10px;' }
    );
    add(
        'input',
        top,
        {
            type: 'button',
            value: 'add a video',
            name: 'butbut',
            onclick: 'makeModal()'
        }
    );
}

document.addEventListener('DOMContentLoaded',  () => {
    makeMainPage();
});
