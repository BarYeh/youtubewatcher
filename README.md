# YouTubeWatcher #

Vannila JS project, click "add a video" and paste a url to a youtube video, this will add a panel with that video, you can set the default size for all videos and the specific size for each video.

### Set up ###

* npm install -g http-server
* clone the repo
* cd to directory
* run http-server

### Notes ###

Was very interesting to do vanilla js, feels like I was reinventing react as I was doing it, but having it be my own was really fun and I would enjoy doing more of this